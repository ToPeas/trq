# 前端
+ javascript框架用的是vue.js,路由用的是vue-router,前端数据状态管理vuex
+ 配合vue.js的组件库是饿了么出品的vue.js的element-ui
+ ajax库用的是axios
+ 前端的项目结构的构建脚本直接使用的是vue.js官方出vue-cli
# 后端

选型用的node.js
框架用的是koa.js


# 数据库

+ mongodb 存储题目的原始信息
+ redis 这种内存数据库暂时存放关于各个用户的答题信息


# 构建

+ docker 进行生产环境的搭建
+ nginx 利用nginx进行正向代理到浏览器能访问的80端口


# 代码版本管理
+ git 代码放在gitlab.com 
