const mongoose = require('mongoose')

const QuestionSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true,
    unique: true
  },
  category: {
    type: String,
    required: true
  },
  rank_1: {
    type: String,
    required: true
  },
  rank_2: {
    type: String,
    required: true
  },
  rank_3: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  options: {
    type: Array,
    required: true
  },
  extras: {
    type: Array,
    required: true
  },
  answer: {
    type: String,
    required: true
  }
})

exports.High = mongoose.model('High', QuestionSchema)
exports.Middle = mongoose.model('Middle', QuestionSchema)
exports.Primary = mongoose.model('Primary', QuestionSchema)
