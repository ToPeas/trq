import koaRouter from 'koa-router'
import users from './user'
import questions from './questions.js';

// console.log (users)
const router = new koaRouter ()

router.prefix ('/api')

router.use (users.routes (), users.allowedMethods ())
router.use (questions.routes (), questions.allowedMethods ())


export default router
