const router = require('koa-router')()
import jwt from '../middlewares/jwt'
import {
  find,
  info,
  jindu,
  submit,
  cuoti,
  submitOne,
  session,
  cuotijindu,
  cuotisubmitOne
} from '../controllers/questions'

router.prefix('/questions')

router
  .get('/questions', find)
  .get('/info', info)
  .get('/jindu', jindu)
  .post('/submit', submit)
  .get('/cuotijindu', cuotijindu)
  .post('/cuotisubmitOne', cuotisubmitOne)  
  // .get('/cuoti', cuoti)
  .post('/submitOne', submitOne)
  .get('/session', session)


// .del('/:id', del)
// .put('/:id', put)

export default router
