import _ from 'lodash'
const { High, Primary, Middle } = require('../models/questions')
const _type = {
  high: 'High',
  primary: 'Primary',
  middle: 'Middle'
}

// 这是模拟考试出题
export const find = async (ctx, next) => {
  // 初级题目
  const { type } = ctx.query
  // 随机的题目
  const data = await require('../models/questions')[_type[type]].aggregate([
    { $sample: { size: 100 } }
  ])
  _.set(ctx.session, ['history', type, 'questions'], data)
  // _.set(ctx.session, ['history', type, 'time'], Date.now())
  return ctx.success('获取成功', { data, time: Date.now() }, 200)
  // next()
}

// 模拟考试提交答案
export const submit = async (ctx, next) => {
  const { type, answers } = ctx.request.body
  // 现在缓存中的题目
  const questions = _.get(ctx.session, ['history', [type], 'questions'], [])
  //  获得错题个数
  const falsesQ = _.remove(questions, (item, index) => {
    return answers[index].choose !== item.answer
  })
  console.log(falsesQ.length)
  const point = 100 - falsesQ.length
  const _false = _.get(ctx.session, ['history', type, 'falsesQ'], [])
  console.log(_false)
  const _ponit = _.get(ctx.session, ['history', type, 'high'], 0)
  _.set(
    ctx.session,
    ['history', type, 'falsesQ'],
    _.uniqBy(_false.concat(falsesQ), 'id')
  )
  console.log(point)
  console.log(_ponit)
  _.set(ctx.session, ['history', type, 'questions'], [])

  _.set(ctx.session, ['history', type, 'high'], point > _ponit ? point : _ponit)
  return ctx.success('success', { point }, 200)
}

// 查看以前做过的错题
export const cuoti = async (ctx, next) => {
  const { type } = ctx.query
  const _data = _.get(ctx.session, ['history', type, 'falsesQ'], [])
  return ctx.success('success', _data, 200)
  await next()
}

// 查看以前做过的题的下一道
export const jindu = async (ctx, next) => {
  const { type } = ctx.query
  const id = _.get(ctx.session, ['history', type, 'done'], 1)
  const total = await require('../models/questions')[_type[type]].count()
  const question = await require('../models/questions')[_type[type]].findOne({ id })
  _.set(ctx.session, ['history', type, 'one'], question)
  // 获取题目成功
  delete question.answer
  ctx.success('success', { question, total }, 200)
  await next()
}

export const submitOne = async (ctx, next) => {
  const { type, answer } = ctx.request.body
  // 获取正在做的是第几题
  const question = _.get(ctx.session, ['history', type, 'one'], false)
  if (!question) {
    return ctx.error('error', {}, 400)
  }
  _.set(ctx.session, ['history', type, 'done'], +question.id + 1)
  if (question.answer !== answer) {
    const falsesQ = _.get(ctx.session, ['history', type, 'falsesQ'], [])
    _.set(ctx.session, ['history', type, 'falsesQ'], [...falsesQ, question])
  }
  ctx.success(
    'success',
    {
      answer: question.answer
    },
    200
  )
  await next()
}

// 这是获得错题的一道
export const cuotijindu = async (ctx, next) => {
  const { type } = ctx.query
  const faslesQ = _.get(ctx.session, ['history', type, 'falsesQ'], [])
  const total = faslesQ.length

  ctx.success('success', { question: faslesQ[0], total }, 200)
  await next()
}

// 提交错题一道
export const cuotisubmitOne = async (ctx, next) => {
  const { type, answer } = ctx.request.body
  // 获取正在做的是第几题
  const faslesQ = _.get(ctx.session, ['history', type, 'falsesQ'], [])
  // const question = _.get(ctx.session, ['history', type, 'one'], false)
  if (faslesQ.length) {
    const question = faslesQ[0]
    if (answer === question.answer) {
      _.set(ctx.session, ['history', type, 'falsesQ'], _.drop(faslesQ, 1))
      return ctx.success(
        'success',
        {
          answer: question.answer
        },
        200
      )
    } else {
      return ctx.success(
        'false',
        {
          answer: question.answer
        },
        200
      )
    }
  }
  await next()
}

// 查看以前答题总体信息
export const info = async (ctx, next) => {
  const { type } = ctx.query
  const id = _.get(ctx.session, ['history', type, 'done'], 0)
  // console.log(id)
  const total = await require('../models/questions')[_type[type]].count()
  // console.log(total)
  //  答题的覆盖率
  const rate = (id / total).toFixed(2) + '%'
  // 错误的覆盖率
  // 获取分数
  const ponit = _.get(ctx.session, ['history', type, 'high'], 0)
  return ctx.success(
    'success',
    {
      done: rate,
      high: ponit
    },
    200
  )
}

export const session = async (ctx, next) => {
  return ctx.success('success', ctx.session, 200)
}
