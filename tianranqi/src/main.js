// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import demo from './plugins/demo'
// import NProgress from 'nprogress' // Progress 进度条
// import 'nprogress/nprogress.css'
import App from './App'
import router from './router'
import store from './store'
import {
  Button,
  Select,
  Option,
  Message,
  Card,
  Radio,
  RadioGroup,
  Tag,
  Row,
  Col,
  Alert,
  Collapse,
  CollapseItem
} from 'element-ui'

Vue.config.productionTip = false

Vue.use(Select)
Vue.use(Button)
Vue.use(Option)
Vue.use(Card)
Vue.use(Radio)
Vue.use(RadioGroup)
Vue.use(Tag)
Vue.use(Row)
Vue.use(Col)
Vue.use(Alert)
Vue.use(Collapse)
Vue.use(CollapseItem)
Vue.use(demo)



Vue.prototype.$message = Message
// Vue.prototype.$confirm = MessageBox

import './style/main.scss'
const unsync = sync(store, router)

router.beforeEach(async (to, from, next) => {
  // if (!store.getters.getQuestionsLen) {
  //   // console.log(111)
  //   store.dispatch('fetchQuestions', { refresh: false }).then(res => {
  //     // console.log(res)
  //     next()
  //   })
  // } else {
  next()
  // }
})

// unsync()
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})


// SESSION:kA5Pe7GO0HYzLD0SYSTJQwT7hBhDImuK
