import fetch from '@/utils/fetch'

// 获取所有的题目

export const fetchQuestions = val => {
  console.info(val)
  return fetch({
    method: 'get',
    url: 'questions/questions',
    params: { type: val }
  })
}

export const fetchFalse = val => {
  return fetch({
    method: 'get',
    url: 'questions/cuoti',
    params: { type: val }
  })
}

export const fetchDone = val => {
  return fetch({
    method: 'get',
    url: 'questions/done',
    params: { type: val }
  })
}

export const fetchInfo = val => {
  return fetch({
    method: 'get',
    url: 'questions/info',
    params: {
      type: val
    }
  })
}

export const submit = ({ type, answers }) => {
  // console.log(amswers)
  return fetch({
    method: 'post',
    url: 'questions/submit',
    data: {
      type,
      answers
    }
  })
}
