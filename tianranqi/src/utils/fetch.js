import axios from 'axios'
import { Message } from 'element-ui'

const baseURL = '/api'

const fetch = axios.create({
  baseURL //
})

fetch.interceptors.request.use(
  config => {
    // if (
    //   config.method === 'post' ||
    //   config.method === 'put' ||
    //   config.method === 'delete' ||
    //   config.method === 'get'
    // ) {
    //   const token = localStorage.getItem('token')
    //   if (token) {
    //     config.headers.Authorization = `Bearer ${token}`
    //   }
    // }
    // config.data = _.cloneDeep(config.data)
    return config
  },
  err => {
    alert(err)
  }
)

fetch.interceptors.response.use(
  response => response,
  error => {
    if (error.response.status === 401) {
      Message.error('对不起，你登录已经失效')
      localStorage.removeItem('token')
      setTimeout(() => {
        history.go(0)
      }, 800)
      return Promise.reject()
    }
    if (error.response.status === 403) {
      Message.error('对不起，你没有此权限')
      return Promise.reject()
    }
    if (error.response.status === 400) {
      Message.error('对不起，参数错误')
      return Promise.reject()
    }
  }
)

export default fetch
