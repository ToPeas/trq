import demo from './demo.vue'

export default {
  install(Vue,options){
    Vue.component('demo', demo)
  }
}

