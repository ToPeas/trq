import Vue from 'vue'
import Vuex from 'vuex'

import { fetchQuestions, fetchInfo, submit, fetchFalse, fetchDone } from '@/api'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    questions: [],
    answer: [],
    info: {},
    type: '',
    falseQ: [],
    false_answer: [],
    done: [],
    done_answer: [],
    time: ''
  },
  mutations: {
    answer(state, answer) {
      if (state.route.params.id) {
        state.answer[state.route.params.id - 1].choose = answer
      }
    },
    false_answer(state, answer) {
      if (state.route.params.id) {
        state.false_answer[state.route.params.id - 1].choose = answer
      }
    },
    done_answer(state, answer) {
      if (state.route.params.id) {
        state.done_answer[state.route.params.id - 1].choose = answer
      }
    },
    setQuestions(state, payload) {
      state.questions = payload.data
      console.log(payload)
      state.answer = payload.data.map(item => {
        return {
          answer: item.answer,
          choose: ''
        }
      })
      state.time = payload.time
    },
    setInfo(state, payload) {
      state.info = payload
    },
    setType(state, payload) {
      state.type = payload
    },
    setFalse(state, payload) {
      state.falseQ = payload
      state.false_answer = payload.map(item => {
        return {
          answer: item.answer,
          choose: ''
        }
      })
    },
    setDone(state, payload) {
      state.done = payload
      state.done_answer = payload.map(item => {
        return {
          answer: item.answer,
          choose: ''
        }
      })
    }
  },
  actions: {
    // 获取要回答的题目
    fetchQuestions({ commit }, { type }) {
      return fetchQuestions(type).then(res => {
        commit('setQuestions', res.data.data)
        commit('setType', type)
        return res.data
      })
    },
    // 获取错题
    fetchFalse({ commit }, { type }) {
      return fetchFalse(type).then(res => {
        commit('setFalse', res.data.data)
        return res.data
      })
    },
    // 获取所有做过的题
    fetchDone({ commit }, { type }) {
      return fetchDone(type).then(res => {
        commit('setDone', res.data.data)
        return res.data
      })
    },
    // 获取在这个难度下的所有情况
    fetchInfo({ commit }, { type }) {
      return fetchInfo(type).then(res => {
        commit('setInfo', res.data.data)
        return res.data.data
      })
    },
    answer({ commit }, { answer }) {
      commit('answer', answer)
    },
    submit({ state }, { type }) {
      return submit({
        type: type,
        answers: state.answer
      }).then(res => res.data)
    }
  },
  getters: {
    getQuestion(state) {
      return state.questions[state.route.params.id - 1] || {}
    },
    getQuestionsLen(state) {
      return state.questions.length
    },
    answers(state) {
      return state.answer
    },
    info(state) {
      return state.info
    }
  }
})

export default store
