export default {
  state: {
    questions: [],
    answers: [
      {
        choose: '',
        answer: ''
      }
    ],
    errors: [],
    errors_answers: [
      {
        choose: '',
        answer: ''
      }
    ],
    done: [],
    done_answers: [
      {
        choose: '',
        answer: ''
      }
    ]
  },
  mutations: {
    SET_ANSWER(state, { answer, index }) {
      state.answers[index].choose = answer
    },
    SET_QUESTIONS(state, payload) {
      state.questions = payload
      state.answers = payload.map(item => {
        return {
          answer: item.answer,
          choose: ''
        }
      })
    },
    SET_FALSE_ANSWER(state, { answer, index }) {
      state.errors_answers[index].choose = answer
    },
    SET_FALSE_QUESTIONS(state, payload) {
      state.errors = payload
      state.errors_answers = payload.map(item => {
        return {
          answer: item.answer,
          choose: ''
        }
      })
    },
    SET_DONE_ANSWER(state, { answer, index }) {
      state.done_answers[index].choose = answer
    },
    SET_DONE_QUESTIONS(state, payload) {
      state.done = payload
      state.done_answers = payload.map(item => {
        return {
          answer: item.answer,
          choose: ''
        }
      })
    }
  },
  actions: {
    // 获取要做的题目
    fetchQuestions({ commit }, { type }) {
      return fetchQuestions(type).then(res => {
        commit('SET_QUESTIONS', res.data.data)
        return res.data
      })
    },
    // 获取做过的错题
    fetchFalse({ commit }, { type }) {
      return fetchFalse(type).then(res => {
        commit('SET_FALSE_QUESTIONS', res.data.data)
        return res.data
      })
    },
    // 获取所有做过的题目
    fetchDone({ commit }, { type }) {
      return fetchFalse(type).then(res => {
        commit('SET_DONE_QUESTIONS', res.data.data)
        return res.data
      })
    },
  },
  getters: {},
  namespaced: true
}
