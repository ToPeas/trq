import fetch from '../utils/fetch'

export default function(option) {
  let state = {
    items: [],
    total: 0
  }

  let actions = {
    fetch({ commit }, { url,  }) {
      return fetch(url, query, pagination).then(response => {
        commit('set', response)
        return Promise.resolve(response)
      })
    },
    findOne({}, { url, id }) {
      return findOne(url, id).then(response => response)
    },
    create({}, { url, payload }) {
      return addOne(url, payload)
    },
    remove({}, { url, id }) {
      return removeOne(url, id)
    },
    edit({}, { url, id, payload }) {
      return editOne(url, id, payload)
    }
  }

  let mutations = {
    set(state, { rows, total }) {
      state.items = rows
      state.total = total
    }
  }
  let getters = {
    items(state) {
      return state.items
    },
    total(state) {
      return +state.total
    }
  }

  return {
    actions,
    mutations,
    getters,
    state,
    namespaced: true
  }
}
