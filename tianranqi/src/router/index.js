import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import History from '@/components/History'
import Question from '@/components/Question'
import Done from '@/components/Done'
import FalseQ from '@/components/FalseQ'

import Confirm from '@/components/Confirm'
import Summary from '@/views/Summary'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/history',
      name: 'History',
      component: History
    },
    {
      path: '/exercise/:id',
      name: 'Done',
      component: Done
    },
    {
      path: '/false/:id',
      name: 'FalseQ',
      component: FalseQ
    },
    {
      path: '/question/:id',
      name: 'Question',
      component: Question
    },
    {
      path: '/confirm/:id',
      name: 'Confirm',
      component: Confirm
    },
    {
      path: '/summary',
      name: 'Summary',
      component: Summary
    }
  ]
})
